inf="$1"
outf="$2"
tag="$1"

pdftk "$inf" dump_data | \
  sed -e 's/\(InfoValue:\)\s.*/\1\ /g' | \
  sed -e 's/\(PdfID[0-9]\+:\)\s.*/\1\ /g' | \
  pdftk "$inf" update_info - output "$outf-tmp"

exiftool -m -all:all= -overwrite_original "$outf-tmp"
qpdf --linearize "$outf-tmp" "$outf"

redact=$(pdftk "$outf" dump_data | \
    grep -e '^PdfID[0-9]\+:\s[0-9a-f]\{32\}' | \
    sed 's/^[^:]\+:\s//'
  )

pdfid=$(echo "$tag" | cat - master_template.svg | sha256sum | cut -d" " -f1 | head -c32)

while read rstr
do
	sed -i "$outf" -e "s/$rstr/$pdfid/"
done <<< "$redact"

rm "$outf-tmp"
