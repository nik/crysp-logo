# Dependencies:
# 0. bash, sed, GNU coreutils
#    -> Pretty much all functionality
# 1. inkscape
#    -> SVG cropping
#    -> SVG to PNG conversion (better support than ImageMagick)
#    -> SVG to PDF conversion
# 2. scour
#    -> SVG compression
# 3. pngcrush
#    -> PNG compression
# 4. ps2pdf & ps2eps (ghostscript)
#    -> PDF compression (Inkscape supports only uncompressed PDFs)
#    -> EPS generation
# 5. convert (ImageMagick)
#    -> multi-resolution favicon creation
# 6. python3
#    -> Custom script for generating the logo layouts from master SVG
# 7. pdftk
#    -> PDF reproducible builds
# 8. exiftool
#    -> PDF reproducible builds
# 9. qpdf
#    -> PDF reproducible builds

OUTDIR := build
ICONDIR := ${OUTDIR}/icon

TEXTVARIANTS := word tagline1 tagline2

SOLIDTEXTBGS := $(TEXTVARIANTS:%=%-whitebg-blackfg) $(TEXTVARIANTS:%=%-blackbg-whitefg) $(TEXTVARIANTS:%=%-mono-blackbg-whitefg) $(TEXTVARIANTS:%=%-mono-whitebg-blackfg)
CLEARTEXTBGS := $(TEXTVARIANTS:%=%-clearbg-blackfg) $(TEXTVARIANTS:%=%-clearbg-whitefg) $(TEXTVARIANTS:%=%-mono-clearbg-blackfg) $(TEXTVARIANTS:%=%-mono-clearbg-whitefg)
SOLIDPICBGS := pictorial-whitebg pictorial-blackbg pictorial-mono-blackbg-whitefg pictorial-mono-whitebg-blackfg
CLEARPICBGS := pictorial-clearbg
SOLIDBGS := ${SOLIDTEXTBGS} ${SOLIDPICBGS}
CLEARBGS := ${CLEARTEXTBGS} ${CLEARPICBGS}
EXTS := $(SOLIDBGS:%=%.svg) $(SOLIDBGS:%=%.png) $(SOLIDBGS:%=%.pdf) $(SOLIDBGS:%=%.eps) $(CLEARBGS:%=%.svg) $(CLEARBGS:%=%.png) $(CLEARBGS:%=%.pdf)
ICONSIZES := 256 228 196 180 167 152 144 128 120 96 76 70 64 57 48 32 24 16
SIMPLEICONSIZES := 64 48 32 16
ICONS := $(ICONSIZES:%=icon-%.png) icon.ico favicon.ico icon.svg
FILEPREFIX := crysp-logo
FILES := $(EXTS:%=${OUTDIR}/${FILEPREFIX}-%) $(ICONS:%=${ICONDIR}/${FILEPREFIX}-%)

.ONESHELL:
SHELL = /bin/bash

all: $(FILES)

${BGS}: ${FILES}

%.svg: %.1.svg
	scour -i "$<" -o "$@" --remove-descriptive-elements --enable-viewboxing --enable-id-stripping --enable-comment-stripping --shorten-ids --indent=none --no-line-breaks

%.1.svg: %.2.svg
	inkscape "$<" --export-area-drawing --export-plain-svg "--export-filename=$@"

%.2.svg:
	mkdir -p ${@D}
	cat master_template.svg | python3 proc-template.py "$(*F)" > "$@"

%.png: %.1.png
	pngcrush "$<" "$@"
	if ((`stat -c%s "$@"` > `stat -c%s "$<"`)) ; then
		cp "$<" "$@"
	fi

%.1.png: %.svg
	f="$@"
	re="^.*-([hw])([0-9]+)\.1\.png$$"
	if [[ $$f =~ $$re ]] ; then
		mode="$${BASH_REMATCH[1]}"
		dim="$${BASH_REMATCH[2]}"
		if [ "$$mode" == "h" ] ; then
			inkscape "$<" --export-type="png" "--export-filename=$@" "--export-height=$$dim"
		else
			inkscape "$<" --export-type="png" "--export-filename=$@" "--export-width=$$dim"
		fi
	else
		inkscape "$<" --export-type="png" "--export-filename=$@"
	fi

%.pdf: %.1.pdf
	/bin/bash strip-pdf-metadata.sh "$<" "$@"

%.1.pdf: %.2.pdf
	ps2pdf -dEPSCrop "$<" "$@"

%.2.pdf: %.svg
	inkscape "$<" --export-type="pdf" "--export-filename=$@"

%.eps: %.1.eps
	cat "$<" | ps2eps | sed -e ':a;N;$$!ba;s/\n%%CreationDate:[^\n]\+\n/\n/g' > "$@"

%.1.eps: %.svg
	inkscape "$<" --export-type="eps" "--export-filename=$@"

TMPICONSRC := ${ICONDIR}/tmp-favicon-icon-clearbg.svg
COMMA := ,
EMPTY :=
SPACE := ${EMPTY} ${EMPTY}

${ICONDIR}/%-icon.svg: ${TMPICONSRC}
	cp "$<" "$@"

# This file is huge because ICOs don't support compressed PNGs; they are always
# RGBA (32 bits per pixel) uncompressed (no palette or anything fancy allowed).
%-icon.ico: ${TMPICONSRC}
	convert -density 384 -background transparent "${TMPICONSRC}" -flatten -colors 256 -define "icon:auto-resize=$(subst ${SPACE},${COMMA},${ICONSIZES})" "$@"

%-favicon.ico: ${TMPICONSRC}
	convert -density 384 -background transparent "${TMPICONSRC}" -flatten -colors 256 -define "icon:auto-resize=$(subst ${SPACE},${COMMA},${SIMPLEICONSIZES})" "$@"

${ICONDIR}/%.1.png: ${TMPICONSRC}
	inkscape "${TMPICONSRC}" "--export-width=$(subst .1.png,,$(subst ${ICONDIR}/${FILEPREFIX}-icon-,,$@))" "--export-height=$(subst .1.png,,$(subst ${ICONDIR}/${FILEPREFIX}-icon-,,$@))" --export-type="png" "--export-filename=$@"

.PHONY: clean
clean:
	rm -rf "${OUTDIR}"
