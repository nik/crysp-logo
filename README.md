# CrySP Logo

This repository contains the official CrySP logo and its associated
build scripts.

The rendered logo is stored in the [build](build/) directory. Filenames
follow this naming convention:

    crysp-logo-X-Y.Z


`X` defines the contents of the logo according to the following table:

| `X` value   | Logo content                                         |
| ----------- | ---------------------------------------------------- |
| `pictorial` | Only the pictorial mark                              |
| `word`      | The pictorial mark and "CrySP"                       |
| `tagline1`  | Pictorial mark, "CrySP", and the lab name            |
| `tagline2`  | Pictorial mark, "CrySP", lab name, and "@ uWaterloo" |


`Y` defines the logo colour scheme according to the following table:

| `Y` value              | Pictorial  | Background  | Text  |
| ---------------------- | ---------- | ----------- | ----- |
| `blackbg-whitefg`      | Tri-colour | Black       | White |
| `whitebg-blackfg`      | Tri-colour | White       | Black |
| `clearbg-blackfg`      | Tri-colour | Transparent | Black |
| `clearbg-whitefg`      | Tri-colour | Transparent | White |
| `mono-blackbg-whitefg` | White      | Black       | White |
| `mono-whitebg-blackfg` | Black      | White       | Black |
| `mono-clearbg-blackfg` | Black      | Transparent | Black |
| `mono-clearbg-whitefg` | White      | Transparent | White |

When `X` is `pictorial`, `Y` is one of the following values:

| `Y` value      | Pictorial  | Background  |
| -------------- | ---------- | ----------- |
| `blackbg`      | Tri-colour | Black       |
| `whitebg`      | Tri-colour | White       |
| `clearbg`      | Tri-colour | Transparent |
| `mono-blackbg` | White      | Black       |
| `mono-whitebg` | Black      | White       |

All combinations are available in SVG, PDF, and PNG formats (based on
the file extension `Z`). Versions that have a solid background colour
(either black or white) are also available in EPS format.


In addition to all of the above, the [build/icon](build/icon) directory
contains versions of the pictorial mark that are usable as icons; they
contain no margins around the mark. Each file named
`crysp-logo-icon-A.png` is a square PNG with `A` pixels per side.
[crysp-logo-icon.ico](build/icon/crysp-logo-icon.ico) is a
multi-resolution icon file containing every resolution from the PNG
files. It is designed to support all of the most common icon sizes
across many devices.
[crysp-logo-favicon.ico](build/icon/crysp-logo-favicon.ico) is
specifically meant for use on the web. It contains only the resolutions
necessary for web use, and thus is significantly smaller than the full
icon file. [crysp-logo-icon.svg](build/icon/crysp-logo-icon.svg) is the
vectorized source for the icons.


The Makefile is also capable of rendering optimized PNGs of specific
sizes on demand. The size of these PNGs is specified either as a fixed
height or a fixed width; the other dimension is automatically
determined. To build such a file, simply append `-hB` or `-wB` to the
filename before the extension, where `B` is the pixel count of the
height or width, respectively. For example:

    make build/crysp-logo-tagline2-clearbg-blackfg-h150.png


Finally, the [master_template.svg](master_template.svg) file in the root
directory contains the Inkscape file that serves as a template for the
Makefile.


Copyright (C) 2018 Cryptography, Security, and Privacy Research Group.
All rights reserved.
