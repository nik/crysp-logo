import sys
import xml.etree.ElementTree as ET
import re

if len(sys.argv) != 2:
	print('Invalid usage; see Makefile')
	sys.exit(1)
operations = re.findall(r'\w+', sys.argv[1])

namespaces = {
	'': 'http://www.w3.org/2000/svg',
	'dc': 'http://purl.org/dc/elements/1.1/',
	'cc': 'http://creativecommons.org/ns#',
	'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
	'svg': 'http://www.w3.org/2000/svg',
	'sodipodi': 'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
	'inkscape': 'http://www.inkscape.org/namespaces/inkscape',
}

for prefix, namespace in list(namespaces.items()):
	ET.register_namespace(prefix, namespace)

tree = ET.parse(sys.stdin)
root = tree.getroot()

def setFillColor(el, c):
	css = el.get('style')
	if css is not None:
		css = re.sub(r'fill:\s*#[0-9a-f]{6};', 'fill:'+c+';', css)
		el.set('style', css)

def setFillOpacity(el, v):
	css = el.get('style')
	if css is not None:
		css = re.sub(r'fill-opacity:[^;]+;', '', css)
		css += ';fill-opacity:'+str(v)+';'
		el.set('style', css)

def deleteElement(tree, el):
	if el is not None:
		for parent in tree.iter():
			if el in parent:
				parent.remove(el)

# Handle deleting unnecessary items
if not 'tagline1' in operations:
	g = root.find('.//svg:g[@id="tagline2v1"]', namespaces)
	if g is not None:
		deleteElement(tree, g)
if not 'tagline2' in operations:
	g = root.find('.//svg:g[@id="tagline2v2"]', namespaces)
	if g is not None:
		deleteElement(tree, g)
if 'tagline1' in operations or 'tagline2' in operations:
	bgName = "backgroundTagline"
else:	
	# Get rid of the first (common) line of the tagline
	deleteElement(tree, root.find('.//svg:g[@id="tagline1"]', namespaces))
		
	# "CrySP" only stays if we're not a pictorial mark
	if 'word' in operations:
		bgName = "backgroundWord"
	else:
		deleteElement(tree, root.find('.//svg:g[@id="crysp"]', namespaces))
		if 'icon' in operations:
			bgName = "backgroundIcon"
		else:
			bgName = "backgroundPictorial"

# We only keep one "background rectangle" based on whether we have a
# tagline or not. This keeps an appropriately sized rectangle so that
# cropping to the drawing works properly.
bg = root.find('.//svg:rect[@id="'+bgName+'"]', namespaces)
for wrongBg in ['backgroundTagline', 'backgroundWord', 'backgroundPictorial', 'backgroundIcon']:
	if wrongBg != bgName:
		deleteElement(tree, root.find('.//svg:rect[@id="'+wrongBg+'"]', namespaces))

# Handle background color
if bg is None:
	print('No background element found')
	sys.exit(1)
if 'whitebg' in operations:
	setFillColor(bg, '#ffffff')
elif 'blackbg' in operations:
	setFillColor(bg, '#000000')
elif 'clearbg' in operations:
	setFillOpacity(bg, 0)

# Handle foreground color
fgColor = None
if 'whitefg' in operations:
	fgColor = '#ffffff'
elif 'blackfg' in operations:
	fgColor = '#000000'
if fgColor is not None:
	# "CrySP" text
	for glyph in root.findall('.//svg:g[@id="crysp"]/svg:path', namespaces):
		setFillColor(glyph, fgColor)
	# Tagline text (full lab name)
	# Some fill styles are set in the groups instead of the glyph paths
	for glyph in root.findall('.//svg:g[@id="tagline"]//*', namespaces):
		setFillColor(glyph, fgColor)
	# The pictorial mark too if requested
	if 'mono' in operations:
		for glyph in root.findall('.//svg:g[@id="pictorial-mark"]/svg:path', namespaces):
			setFillColor(glyph, fgColor)

# Strip font data from text that has been converted to paths
for el in root.findall('.//*[@style]', namespaces):
	css = el.get('style')
	css = re.sub(r'font-(?:feature-settings|variant-caps|variant-ligatures|variant-numeric):[^;"]+[;"]', '', css)
	el.set('style', css)

tree.write(sys.stdout, xml_declaration=True, encoding='unicode')
